
SHELL:=/bin/bash

all: main.cpp parser_helpers.cpp deduce_logic.cpp
	g++ -std=c++11 parser_helpers.cpp deduce_logic.cpp main.cpp -o parseCnf -lboost_system -lboost_filesystem  

and: all
	./parseCnf -f ./test/and.cnf -i 2 -o 1
or:  all
	./parseCnf -f ./test/or.cnf  -i 2 -o 1
xor: all
	./parseCnf -f ./test/xor.cnf  -i 2 -o 1

nand: all
	./parseCnf -f ./test/nand.cnf  -i 2 -o 1
nor:  all
	./parseCnf -f ./test/nor.cnf  -i 2 -o 1
xnor: all
	./parseCnf -f ./test/xnor.cnf  -i 2 -o 1


test: and or xor nand nor xnor

clean:
	rm -f parseCnf


