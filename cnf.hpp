// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : cnf.hpp
// @brief  : A simple class to store the CNF object
//
// The class is full visible, and rather than efficiency, correctness is the focus.
//------------------------------------------------------------------------------


#pragma once
#ifndef CNF_HPP
#define CNF_HPP

#include <string>
#include <vector>
#include <unordered_set>

namespace axekit {

using Lit = int;

using Clause = std::vector<Lit>; // An N literal clause

class CNF {
public:
  // assigns are not clauses. 
  std::vector <Clause> assigns; // All assignments i.e. lit0 = lit2 OR lit2 OR lit3 ...
  std::vector <Lit> clauses_1Lit; // list of all 1 lit clauses (constants or outputs)
  std::vector <Clause> clauses; // list of all multi literal clauses.
  std::unordered_set <Lit> literals;  // unique list of all the literals.
  std::string filename;
  Lit max_1LitClause_lit = 0; // This will be the aXc constant 
  int nLiterals; 
  int nClauses;
  int nInputs  = 0;
  int nOutputs = 0;
  std::unordered_set <Lit> usedPi; // always unique
  std::unordered_set <Lit> usedPo; // always unique
  std::vector <std::string> comments;

private:
  
  
};

}


#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
