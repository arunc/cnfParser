

// writes everything as AND, XOR,
// This is sufficient for all 2 input gates (AND, NAND, OR, NOR, XOR, XNOR)
// and INV, ASSIGN


#include "deduce_logic.hpp"

using namespace boost::assign;

namespace axekit {


// efficiency is of least concern!
// 1st procedure
// changes the sign also from the CNF
// i.e.  -a = -b ----> a = b
//       -a =  b ----> a = -b
void deduce_unary_assigns ( std::vector<Clause> &assigns, std::vector<Clause> &clauses ) {
  std::vector <unsigned> erase_positions;
  for (auto i=0u; i < clauses.size() - 1; i++) {
    if ( clauses[i].size() != 2 ) continue;
    if ( clauses[i+1].size() != 2 ) continue; 
    auto x1 = (clauses[i])[0];
    auto y1 = (clauses[i])[1];
    auto x2 = (clauses[i+1])[0];
    auto y2 = (clauses[i+1])[1];

    Lit xx, yy;
    
    if ( 0 == (x1 + x2) && 0 == (y1 + y2) )  { // assign or invertor
      auto x = (x1 > 0) ? x1 : -x1;
      auto y = (y1 > 0) ? y1 : -y1;
      xx = x;
      if ( (x1 < 0 && y1 < 0) || (x2 < 0 && y2 < 0)  )
	yy = -y; // inv
      else
	yy = y; // normal assign.

      if ( xx < 0 ) { xx = -xx; yy = -yy;} // change the sign.
      assigns.emplace_back ( std::vector<Lit> {xx, yy} ); 
      erase_positions.emplace_back (i);
      erase_positions.emplace_back (i+1);
    }
  }

  for (auto i : erase_positions) {
    clauses[i] = std::vector<Lit> {0}; // Mark as 0.
    //clauses.erase ( clauses.begin() + i);  // not Ok, after erasing, it will rearrange.
  }

}


// 2nd procedure
// AND
// clauses and order 
// cls0 :: (-x1 + y1) *        OR       (x3 + -y3 + -z3) *
// cls1 :: (-x2 + z2) *		        (-x2 + z2) 
// cls2 :: (x3 + -y3 + -z3)	        (-x1 + y1) *             
void deduce_AndGates ( std::vector<Clause> &assigns, std::vector<Clause> &clauses ) {
  std::vector <unsigned> erase_positions;
  for (auto i=0u; i<clauses.size(); i++) {
    // 1. take 3 clauses at a time.
    if ( (i + 2) >= clauses.size() ) break; 
    auto cls0 = clauses[i];
    auto cls1 = clauses[i + 1];
    auto cls2 = clauses[i + 2];
    bool cond1 = false;
    bool cond2 = false;
    if ( (cls0.size() == 2) && (cls1.size() == 2) && (cls2.size() == 3) ) cond1 = true;
    if ( (cls0.size() == 3) && (cls1.size() == 2) && (cls2.size() == 2) ) cond2 = true;
    if ( !(cond1 || cond2) ) continue; // never bother about too jumbled order.

    auto x1 = cond1 ? cls0[0] : cls2[0];
    auto x2 = cls1[0];
    auto x3 = cond1 ? cls2[0] : cls0[0];
    auto y1 = cond1 ? cls0[1] : cls2[1];
    auto y3 = cond1 ? cls2[1] : cls0[1];
    auto z2 = cls1[1];
    auto z3 = cond1 ? cls2[2] : cls0[2];

    if ( 0 == ( (x1+x2) + 2*x3 )  && 0 == ( y1 + y3 + z2 + z3 ) ) { // this is a match
      assigns.emplace_back ( std::vector<Lit> {x3, y1, z2, 0} );  // x3 = y1 . z2  (last 0 for AND)
      erase_positions.emplace_back (i); // remove these clauses.
      erase_positions.emplace_back (i + 1);
      erase_positions.emplace_back (i + 2);
    }
    
  }

  for (auto i : erase_positions) {
    clauses[i] = std::vector<Lit> {0}; // Mark as 0.
    //clauses.erase ( clauses.begin() + i);
  }
  
}



// 3rd procedure
// XOR
void deduce_XorGates ( std::vector<Clause> &assigns, std::vector<Clause> &clauses ) {
  std::vector <unsigned> erase_positions;
  for (auto i=0u; i<clauses.size(); i++) {
    // 1. take 4 clauses at a time.
    if ( (i + 3) >= clauses.size() ) break; 
    auto cls0 = clauses[i];
    auto cls1 = clauses[i + 1];
    auto cls2 = clauses[i + 2];
    auto cls3 = clauses[i + 3];

    if ( !( (cls0.size() == 3) && (cls1.size() == 3) &&
	    (cls2.size() == 3) && (cls3.size() == 3) ) ) continue;

    auto x0 = cls0[0]; auto x1 = cls1[0]; auto x2 = cls2[0]; auto x3 = cls3[0];
    auto y0 = cls0[1]; auto y1 = cls1[1]; auto y2 = cls2[1]; auto y3 = cls3[1];
    auto z0 = cls0[2]; auto z1 = cls1[2]; auto z2 = cls2[2]; auto z3 = cls3[2];
    auto xx = x0 + x1 + x2 + x3;
    auto yy = y0 + y1 + y2 + y3;
    auto zz = z0 + z1 + z2 + z3;

    if ( 0 == xx && 0 == yy && 0 == zz ) { // this is a match
      auto x = x0 < 0 ? -x0 : x0;
      auto y = y0 < 0 ? -y0 : y0;
      auto z = z0 < 0 ? -z0 : z0;
      assigns.emplace_back ( std::vector<Lit> {x, y, z, 1} );  // x = y xor z (last 0 for XOR)
      erase_positions.emplace_back (i); // remove these clauses.
      erase_positions.emplace_back (i + 1);
      erase_positions.emplace_back (i + 2);
      erase_positions.emplace_back (i + 3);
    }
    
  }

  for (auto i : erase_positions) {
    clauses[i] = std::vector<Lit> {0}; // Mark as 0.
    //clauses.erase ( clauses.begin() + i);
  }
  
}






}



// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
