// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : deduce_logic.hpp
// @brief  : deduce some obvious stuffs such as inv, and, or, simple assignment etc.
//           This is useful only for debugging and provide hints what the vars are.
// 
// Works by finding the pattern of inversion among literals in consecutive clauses. 
//------------------------------------------------------------------------------


#pragma once
#ifndef DEDUCE_LOGIC_HPP
#define DEDUCE_LOGIC_HPP

#include <iostream>
#include <string>
#include <vector>
#include <unordered_set>
#include <boost/assign.hpp>
#include "cnf.hpp"

namespace axekit {

void deduce_unary_assigns ( std::vector<Clause> &assigns, std::vector<Clause> &clauses );
void deduce_AndGates ( std::vector<Clause> &assigns, std::vector<Clause> &clauses );
void deduce_XorGates ( std::vector<Clause> &assigns, std::vector<Clause> &clauses );

}


#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
