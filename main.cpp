
// Just parse the CNF and print in the formula form.
// For debugging and as a starting point for our own parser.


#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <vector>
#include <unordered_set>
#include <cstdlib>
#include <cassert>

#include <boost/assign.hpp>
#include <boost/filesystem.hpp>
#include <boost/lexical_cast.hpp>

#include "cnf.hpp"
#include "simple_cli.hpp"
#include "parser_helpers.hpp"
#include "deduce_logic.hpp"

using namespace boost::assign;

void printUsage () {
  std::cout << "[i] Usage ./parseCnf -i <num_inputs> -o <num_outputs> -cnftool <0/1/2> -f <cnffile>\n";
  std::cout << "           -i and -o are optional. \n";
  std::cout << "           -cnftool (optional) is the tool that generated CNF. 1=aXc, 0=none\n";
  std::cout << "            aXc CNF : Outputs, Gates, Constants/Outputs-constraints, then Inputs.\n";

}

//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------

int main (int argc, char **argv) {
  CLIParser cli (argc, argv);
  const std::string file = cli.getCmdOption ("-f");
  if ( cli.cmdOptionExists ("-h") || file.empty() ) { printUsage();  return 0;}

  boost::filesystem::path p (file);
  if ( !boost::filesystem::exists(p) ) {
    std::cout << "[e] Input file does not exist/cannot open!\n";
    return -1;
  }
  
  auto pi  = cli.getCmdOption ("-i");
  auto po  = cli.getCmdOption ("-o");
  int style = 1;
  if ( cli.cmdOptionExists ("-cnftool") )
    style = axekit::string_to_num ( cli.getCmdOption ("-cnftool") );
  
  axekit::CNF cnf;
  cnf.filename = std::string ( file );
  std::ifstream fs (cnf.filename);
  if ( !pi.empty() ) { cnf.nInputs  =  axekit::string_to_num (pi); }
  if ( !po.empty() ) { cnf.nOutputs =  axekit::string_to_num (po); }

  int cnt = 0;
  for ( std::string line; std::getline (fs, line); ) {
    cnt++;
    if ( line[0] == 'c' ) { cnf.comments += line; continue; }
    if ( line[0] == 'p' ) { axekit::parseProblemLine (cnf, line); continue; }
    axekit::parseClauseLine (cnf, line, cnt);
      
  }

  //axekit::printCurrentCnf (cnf) ;
  
  // Do some logic deduction.
  axekit::deduce_unary_assigns ( cnf.assigns, cnf.clauses );
  //axekit::printCurrentCnf (cnf) ;
  axekit::deduce_AndGates ( cnf.assigns, cnf.clauses );
  //axekit::printCurrentCnf (cnf) ;
  axekit::deduce_XorGates ( cnf.assigns, cnf.clauses );

  if (style == 1) {
    axekit::printClauses_withIO (cnf);
    axekit::printAssigns_withIO (cnf);
  }
  else {
    axekit::printClauses (cnf);
    axekit::printAssigns (cnf);    
  }
  //---if ( cnf.abc_style )
  //---  axekit::printClauses_withIO (cnf);
  //---else
  //---  axekit::printClauses (cnf);
  //---axekit::printInfo (cnf);
  
  return 0;
}


// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
