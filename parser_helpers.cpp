

#include "parser_helpers.hpp"


using namespace boost::assign;

namespace axekit {

// http://stackoverflow.com/questions/236129/split-a-string-in-c
inline void splitLine (const std::string &str, char delim, std::vector<std::string> &elems) {
  std::stringstream ss;
  ss.str(str);
  std::string item;
  while ( std::getline(ss, item, delim) ) {
    if (! item.empty() )
      elems.push_back (item);
  }
}
inline std::vector<std::string> splitLine (const std::string &str, char delim) {
  std::vector <std::string> elems;
  splitLine (str, delim, elems);
  return elems;
}


inline std::string make_var (const Lit &lit) {
  if ( lit < 0 )
    return std::string ( "!x" + std::to_string (-lit) );
  return std::string ( "x" + std::to_string (lit) );
}


inline std::string make_axc_var (const CNF &cnf, const Lit &lit) {
  auto pos_lit = lit < 0 ? -lit : lit;
  if ( pos_lit > cnf.max_1LitClause_lit )  { // Input literal
    if ( lit < 0 )
      return std::string ( "!i" + std::to_string (-lit) );
    return std::string ( "i" + std::to_string (lit) );
  }
  if ( pos_lit <= cnf.nOutputs ) { // Output literal
    if ( lit < 0 )
      return std::string ( "!o" + std::to_string (-lit) );
    return std::string ( "o" + std::to_string (lit) );
  }
  
  if ( lit < 0 ) // Gate literal.
    return std::string ( "!x" + std::to_string (-lit) );
  return std::string ( "x" + std::to_string (lit) );
}


int string_to_num ( const std::string &str ) {
  try {
    auto num = boost::lexical_cast <int> (str);
    return num;
  }
  catch ( boost::bad_lexical_cast & ) {
    std::cout << "[e] The token : " << str << " is NOT a valid number!\n";
    exit (-3);
  }
}


void parseProblemLine ( CNF &cnf, const std::string &line ) {
  assert (line[0] == 'p' && line[1] == ' ' && "What line is this?");
  const auto words = splitLine ( line, ' ');
  if ( words.size() != 4 ) {
    std::cout << "[e] Malformed problem line : " << line << "\n";
    exit (-2);
  }
  if ( words[1] != "cnf" )  {
    std::cout << "[e] Supports only CNF problems!\n";
    exit (-2);
  }
  cnf.nLiterals = string_to_num ( words[2] );
  cnf.nClauses  = string_to_num ( words[3] );
  
}



void parseClauseLine ( CNF &cnf, const std::string &line, const int lineCount) {
  if ( line.empty() ) return;
  if ( lineCount > (cnf.nClauses + cnf.comments.size() + 1) ) {
    std::cout << "[w] Number of clauses exceeds specification (given " << cnf.nClauses
	      << ") in problem statement. Extra clauses discarded.\n";
    return;
  }
  const auto words = splitLine (line, ' ');
  if ( words.back() != "0" ) {
    std::cout << "[e] The last token in line " << lineCount << " is NOT 0\n";
    exit (-4);
  }

  if ( 1 == words.size() ) {
    std::cout << "[w] Cannot parse line " << lineCount << ". Discarding\n";
    return;
  }
  
  if ( 2 == words.size() ) { // constant or output literal.
    auto lit = string_to_num ( words[0] );
    auto pos_lit = lit < 0 ? -lit : lit;
    if ( pos_lit > cnf.max_1LitClause_lit ) cnf.max_1LitClause_lit = pos_lit;
    cnf.clauses_1Lit.emplace_back ( lit );
    return;
  }
  
  Clause cls;
  for (auto i=0u; i < words.size() - 1; i++) {
    auto num = string_to_num ( words[i] );
    if ( num == 0 ) {
      std::cout << "[e] 0 as a literal NOT allowed in line " << lineCount << "\n";
      exit (-4);
    }
    cnf.literals.insert (num);
    cls += num;
  }
  cnf.clauses += cls;
}

void printCurrentCnf ( const CNF &cnf ) {
  for ( auto &cls : cnf.clauses ) {
    for (auto &lit : cls) {
      std::cout << lit << " ";
    }
    std::cout << "\n";
  }
  
}

void printClauses ( const CNF &cnf ) {
  for ( auto &cls : cnf.clauses ) {
    // 1Lit clauses in clauses list means these are processed clauses.
    if (cls.size() == 1) continue; 
    for (auto i=0u; i < cls.size() - 1 ; i++) {
      auto lit = cls[i];
      assert (lit != 0 && "Literal 0 not allowed");
      std::cout << make_var (lit) << " * ";
    }
    auto litlast  = cls [cls.size() - 1];
    std::cout << make_var (litlast) << "    + \n";
  }
  std::cout << "\n";

  for (auto &cls : cnf.clauses_1Lit ) { // 1Lit clauses.
    if (cls < 0)
      std::cout << "x" << -cls << " = " << 0 << " // output or constant \n";
    else
      std::cout << "x" <<  cls << " = " << 1 << " // output or constant \n";
  }

}


void printClauses_withIO ( const CNF &cnf ) {
  for ( auto &cls : cnf.clauses ) {
    // 1Lit clauses in clauses list means these are processed clauses.
    if (cls.size() == 1) continue; 
    for (auto i=0u; i < cls.size() - 1 ; i++) {
      auto lit = cls[i];
      assert (lit != 0 && "Literal 0 not allowed");
      std::cout << make_axc_var (cnf, lit) << " * ";
    }
    auto litlast  = cls [cls.size() - 1];
    std::cout << make_axc_var (cnf, litlast) << "    + \n";
  }
  std::cout << "\n";

  for (auto &cls : cnf.clauses_1Lit ) { // 1Lit clauses.
    if (cls < 0)
      std::cout << "x" << -cls << " = " << 0 << " // output or constant \n";
    else
      std::cout << "x" <<  cls << " = " << 1 << " // output or constant \n";
  }

}


void printAssigns ( const CNF &cnf ) {
  for ( auto line : cnf.assigns ) {
    if ( 2 == line.size() ) {
      std::cout << make_var ( line[0] ) << " = " << make_var ( line[1] ) << "\n";
    }
    else if ( 4 == line.size() ) {
      std::cout << make_var ( line[0] ) << " = ";
      if ( line[3] == 0 )
	std::cout << make_var ( line[1] ) << " * " << make_var ( line[2] ) << "    (AND) \n";
      else
	std::cout << make_var ( line[1] ) << " ^ " << make_var ( line[2] ) << "    (XOR) \n";
    }
    else {
      assert (false);
    }
  }
}


void printAssigns_withIO ( const CNF &cnf ) {
  for ( auto line : cnf.assigns ) {
    if ( 2 == line.size() ) {
      std::cout << make_axc_var ( cnf, line[0] ) << " = " << make_axc_var ( cnf, line[1] ) << "\n";
    }
    else if ( 4 == line.size() ) {
      std::cout << make_axc_var ( cnf, line[0] ) << " = ";
      if ( line[3] == 0 )
	std::cout << make_axc_var ( cnf, line[1] ) << " * " << make_axc_var ( cnf, line[2] ) << "    (AND) \n";
      else
	std::cout << make_axc_var ( cnf, line[1] ) << " ^ " << make_axc_var ( cnf, line[2] ) << "    (XOR) \n";
    }
    else {
      assert (false);
    }
  }
}


//---void printClauses_withIO ( CNF &cnf ) {
//---  assert (cnf.abc_style);
//---  auto k_lit = cnf.const_lit > 0 ? cnf.const_lit : -cnf.const_lit;
//---  std::string name;
//---  if ( cnf.nOutputs >= k_lit ) {
//---    std::cout << "[e] An output variable cannot go beyond constant!\n";
//---    return;
//---  }
//---  
//---  for ( auto &cls : cnf.clauses) {
//---    for (auto i=0u; i < cls.size() - 1 ; i++) {
//---      auto lit = cls[i];
//---      assert (lit != 0 && "Literal 0 not allowed");
//---      auto var = lit > 0 ? lit : -lit;
//---      auto sign = lit > 0 ? " " : "!";
//---
//---      if (var <= cnf.nOutputs)
//---	name = "o" + std::to_string (var);
//---      else if (var == k_lit )
//---	name = "k" + std::to_string (var);
//---      else if (var > k_lit) {
//---	name = "i" + std::to_string (var);
//---	cnf.usedPi.insert (var);
//---      }
//---      else
//---	name = "x" + std::to_string (var);
//---      
//---      std::cout << sign << name  << " * ";
//---    }
//---
//---    auto litx  = cls [cls.size() - 1];
//---    auto signx = litx > 0 ? " " : "!";
//---    auto varx  = litx > 0 ? litx : -litx;
//---    if (varx <= cnf.nOutputs)
//---      name = "o" + std::to_string (varx);
//---    else if (varx == k_lit )
//---      name = "k" + std::to_string (varx);
//---    else if (varx > k_lit) {
//---      name = "i" + std::to_string (varx);
//---      cnf.usedPi.insert (varx);
//---    }
//---    else
//---      name = "x" + std::to_string (varx);
//---    
//---    std::cout << signx << name << "    + \n";
//---  }
//---  std::cout << "\n";
//---  if (cnf.const_lit < 0)
//---    std::cout << "k" << -cnf.const_lit << " = " << 0 << "\n";
//---  else
//---    std::cout << "k" << cnf.const_lit << " = " << 1 << "\n";
//---  
//---}



void printInfo  ( const CNF &cnf ) {
  //---std::cout << "\nOther Info : \n";
  //---if (cnf.abc_style) {
  //---  if (cnf.usedPi.size() != cnf.nInputs  && cnf.nInputs != 0) {
  //---    std::cout << "[i] Num inputs specified (=" << cnf.nInputs << " and used in CNF (="
  //---		<< cnf.usedPi.size() << ") are different\n";
  //---  }
  //---  std::cout << "[i] Num inputs used in CNF = " << cnf.usedPi.size() << "\n";
  //---  if ( cnf.nOutputs != 0 ) 
  //---    std::cout << "[i] Num outputs in CNF = " << cnf.nOutputs << "\n";
  //---}
  //---
  //---if ( cnf.nLiterals > (cnf.literals.size() - 1) )  {
  //---  std::cout << "[w] " << ( cnf.nLiterals - cnf.literals.size() + 1)
  //---	      << " variables are unused in the CNF\n";
  //---}
  //---if ( cnf.nLiterals < (cnf.literals.size() - 1) )  {
  //---  std::cout << "[e] " << ( (cnf.literals.size() - 1) - cnf.nLiterals )
  //---	      << " number of vars appear more than specified in the problem line!\n";
  //---}
  //---if ( cnf.nClauses != (cnf.clauses.size() + 1) )  {
  //---  std::cout << "[e] Mismatch in num clauses in the problem line (=" << cnf.nClauses 
  //---	      << ") vs actual number of clauses (=" << (cnf.clauses.size() + 1) << ")!\n";
  //---}

}


}


// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
