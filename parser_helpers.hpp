// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : parser_helpers.hpp
// @brief  : A simple commandline parser
//
//------------------------------------------------------------------------------

#pragma once
#ifndef PARSER_HELPERS_HPP
#define PARSER_HELPERS_HPP

#include <string>
#include <vector>
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <vector>
#include <unordered_set>
#include <cstdlib>
#include <cassert>

#include <boost/assign.hpp>
#include <boost/filesystem.hpp>
#include <boost/lexical_cast.hpp>


#include "cnf.hpp"

namespace axekit {
  
int string_to_num ( const std::string &str );
void parseProblemLine ( CNF &cnf, const std::string &line );
void parseClauseLine ( CNF &cnf, const std::string &line, const int lineCount);
  
void printClauses ( const CNF &cnf );
void printAssigns ( const CNF &cnf );
void printClauses_withIO ( const CNF &cnf );
void printAssigns_withIO ( const CNF &cnf );
void printInfo  ( const CNF &cnf );
void printCurrentCnf ( const CNF &cnf );
}

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
