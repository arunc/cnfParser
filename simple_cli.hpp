// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : simple_cli.hpp
// @brief  : A simple commandline parser
//
// Original taken from here:
// http://stackoverflow.com/questions/865668/how-to-parse-command-line-arguments-in-c
//------------------------------------------------------------------------------
//
/* Usage

  CLIParser cli (argc, argv);

  if ( cli.cmdOptionExists ("-h") ) { doSomething();}
  auto pi  = cli.getCmdOption ("-i");
  if ( !pi.empty() ) { doSomething(); }
  if ( cli.cmdOptionExists ("-noAbc") ) { abc = false; }


 */

#pragma once
#ifndef SIMPLE_CLI_HPP
#define SIMPLE_CLI_HPP

#include <string>
#include <vector>

class CLIParser{
public:
  CLIParser (int &argc, char **argv){
    for (int i=1; i < argc; ++i)
      this->tokens.push_back(std::string(argv[i]));
  }
  /// @author iain
  const std::string getCmdOption(const std::string &option) const{
    std::vector<std::string>::const_iterator itr;
    itr =  std::find(this->tokens.begin(), this->tokens.end(), option);
    if (itr != this->tokens.end() && ++itr != this->tokens.end()){
      return *itr;
    }
    return std::string ("");
  }
  /// @author iain
  bool cmdOptionExists(const std::string &option) const{
    return std::find(this->tokens.begin(), this->tokens.end(), option)
      != this->tokens.end();
  }
private:
  std::vector <std::string> tokens;
};



#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
